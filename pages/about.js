
import Layout from '../components/Layout'

export default function AboutPage() {
  return (
    <Layout title='Sobre'>
      <h1 className='text-5xl border-b-4 font-bold'>Sobre</h1>

      <div className='bg-white shadow-md rounded-lg px-10 py-6 mt-6'>
        <h3 className='text-2xl mb-5'>Diego Capella Blog</h3>
        <p>Este blog foi construido com Next.JS e Markdown</p>
        <p>
          <span className='font-bold'>Versão: 1.0.0</span>
        </p>
      </div>
    </Layout>
  )
}